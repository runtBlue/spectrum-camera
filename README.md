これは、学部卒業論文の作業ディレクトリです。大体これで決定する。

```
├── Guardfile # rake をまわしたいときに使う監視
├── README.md # これ
├── Rakefile # rakeの起動
├── _materials # 作業用ディレクトリ、ここで決まってから .src に挿入していく。
├── build # 完成品
├── figures # 使う画像の完成品、いじらない
├── figures.src # 使うと決まった画像を置く。（.pdf, .jpg, .png, .eps）
├── lib # タスクファイルとそのパーチャルディストリビューション、マクロファイル
├── mediabb.sty # .pdf に大きさ情報を与えるやつ。作業はtaskで書かれてある。
├── sections # セクションごとの .tex ファイル。さわらない。
├── sections.src # セクションごとのmarkdown ファイル。.tex のために特殊な記法があるので学習すること。
└── thesis.tex # 論文メインのtex。セクションのみ分割。マクロを lib に依存してる。
```

これはなに
---
$rake にいろいろと用意して、.jpg .png画像や.pdf図から .pdf + .xbb の自動生成、ファイル名ベースのマクロの自動生成、キャプションの分割化を行う作業フォルダ。
自動化は ruby を使用している。

自動化のコマンド一覧は $rake -T で見ることができます。rake の実体は ./lib/tasks/ 以下にあります。

マークダウンからテフへ
---
./sections.src 以下の .md を編集して執筆する。
それらが ./sections/ 以下に自動生成される。この .tex を適切に使うために ./thesis.tex に input を記述すること。
マークダウンの変換から .pdf のビルドまでを $rake で行うことができる。

ファイルの編集監視と自動ビルド
---
ruby の Guard を使って .pdf の自動ビルドができます。

$guard で自動ビルドが立ち上がります。
監視中に return キーを押すと強制更新できます。
ctrl + C or ctrl + Dで終了できます。

詳しくは Guard の使い方を調べてください。
監視対象などの設定は ./Guardfile にあります。

マクロ自動生成
---
figures.src 以下に画像やpdf図を置いてください。
ただし、__ ファイル名は小文字アルファベット始まりのハイフン接続で命名することを強制します。__
それらは $rake embody で figures に自動生成されます。
TEXで使用する図は ./figures にあるものだけです。

ファイル名から自動的にマクロが生成されています。
現在しようすることができるマクロ一覧は $rake catalog で見れます。

使用例

- hoge-huga.jpg を figures.src に置く。
- $rake embody で figures に移動される、.xbbが生成される。
  - このとき、マクロ"\\hogehuga"が使えるようになっている。実体は ./lib/macro/figures.tex に存在している。

自動生成されたマクロは、すべて ./lib 以下にある。

自動生成された際に、キャプションが仮の形で自動挿入されている。
このキャプションの編集を行うためには、./lib/captions.yml ファイルを編集する。
これはYAML形式で、マクロ名: "キャプション内容" とすればキャプションを追加できる。
追加したキャプションは $rake catalog で更新される。

使用例

- "\\hogehuga" が使えることを確認する。
- ./lib/captions.yml を編集する。
  - 以下を追加する。「hogehuga: "ほげふが"」
- $rake catalog でマクロが更新される。

実際に執筆に用いるとき、ファイル名のままだと可読性が落ちるため、別のマクロ名をつけることを推奨する。
マクロ名のつけ方は、「先ほどの命名ルール」@「マクロ名」
できる限りマクロは大文字から始まる、中身がなんなのかわかりやすい名前をつけること。これは自動生成されたマクロと区別するため。

使用例

- hoge-huga@HogeHuga.jpg に名前を変更する。
- (./figures 以下をすべて削除する。)
- $rake embody
- ./lib/captions.yml に「HogeHuga: "ハゲ"」を追加する
- $rake catalog でキャプションが更新される。

自動生成マクロは[htbp]オプションで挿入されているので、セクションの最後の図が表示されます。
強制挿入の[H]を使いたいときや、サイズの変更を行いたい場合、マクロの上書きをする必要があります。

マクロの上書き用のファイルは ./lib/macro/superscript.tex にあります。

使用例

- ./lib/macro/figures.tex から \\HogeHuga が定義されている部分を_コピー_する。
- ./lib/macro/superscript.tex にペーストする。これらを編集すれば適応される。

> def で強制上書きしていますが、命名規則に習えば衝突することはないと思います。気になる方は ./lib/tasks/ を編集してください。

現在使える図の一覧、写真の一覧、マクロの一覧を見渡すことができるカタログを自動生成している。
カタログは ./lib/catalog/ 以下にある。カタログは .pdf の変換時に同期されないことがあるため、そのときは $rake catalog -c で更新する。

その他メモ
---

何かあった時のために、成功したコマンドのメモ

```
  simpdftex uplatex --mode dvipdfmx  --extratexopts "-synctex=1 -kanji=utf-8 --interaction=nonstopmode" original.tex
後に必要になるであろう記述
  % \bibliographystyle{unsrt}
  % \bibliography{bib/thesis}\pagenumbering{alph}
  % \listoftables
  % \listoffigures
削除した input-name
  abst_eng, abst, intro, seaquest, spec, drift, drift_str, const, crosstalk, track, eff, reso, conclusion, conc, app
```

