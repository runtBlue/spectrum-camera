# encoding: utf-8
# このファイルの問題点
#   廃棄したファイルをなにかあったときのために残すことは悪いことではないと思うが、
#   どういう点で、再利用性があるのか明記すべきだった。

catalog_name = "lib/catalog/figures-catalog.md"

sources = {
  :figure => ["figures.src/*.png", "figures.src/*.eps", "figures.src/*.jpg"]
}

task :catalog_deprecated do
  results = []
  Dir.glob(sources[:figure]).each {|raw|

    pdf = raw.sub(".eps", ".pdf").sub(".png", ".pdf").sub(".jpg", ".pdf").sub(".src", "")
    jpg = raw.sub(".eps", ".jpg").sub(".png", ".jpg").sub(".src", "")

    p `convert -density 144 #{raw} #{jpg}`
    p `convert -density 144 #{raw} #{pdf}`

    template_label = pdf.sub(/.*\//, "")

    template = """
\\begin{figure}[htbp]
\\centering
\\includegraphics[width = 10.0cm]{#{File.expand_path(pdf)}}
\\caption{#{template_label}}
\\end{figure}
""".chomp

    results.push ""
    results.push "```"
    results.push "![#{pdf}](#{File.expand_path(pdf)})"
    results.push "```"
    results.push ""
    results.push "```"
    results.push template
    results.push "```"
    results.push ""
    results.push "![](#{File.expand_path(jpg)})"
  }

  result_log = results.join("\n")
  File.write catalog_name, result_log
  `open #{catalog_name}`
end
