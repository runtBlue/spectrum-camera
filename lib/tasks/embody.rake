# encoding: utf-8

require 'yaml'

config = YAML.load_file( File.expand_path( File.dirname(__FILE__) + '/../config.yml' ) )["embody"]

# requirements end

desc "図・画像を .pdf に変換し、それぞれに .xbb ファイルを与える。"
task :embody => ["embody_old", "embody_pdf"]


task :embody_old do
  Dir.glob(config["src"]).each do |raw|

    pdf = config["dest"] + File.basename(raw, File.extname(raw)).to_s + ".pdf"
    `convert -density 144 #{raw} #{pdf}`
    puts pdf
  end
  `extractbb #{config["dest"]}*.pdf`
end

task :embody_pdf do
  Dir.glob(config["copy_src"]).each do |pdf|
    dest = config["dest"]
    `cp #{pdf} #{dest}`
  end
  `extractbb #{config["dest"]}*.pdf`
end