# encoding: utf-8

require 'yaml'
config = YAML.load_file( File.expand_path( File.dirname(__FILE__) + '/../config.yml' ) )["catalog"]
captions = YAML.load_file( File.expand_path( File.dirname(__FILE__) + '/../captions.yml' ) )

# requirements end


desc "カタログを作成。マクロを更新。(-c : optional to compile)"
task :catalog do |t, args|

  results = []
  macros = []

  Dir.glob(config["src"]).map do |pdf|

    basename = File.basename(pdf, ".pdf")
    submacro = basename.gsub(/([- 0-9])/, "")
    macro = basename.split("@")[1] || submacro
    jpg = pdf.sub(".pdf", ".jpg")

    if ARGV.last.to_s == "-c"
      `convert -density 144 #{pdf} #{jpg}`
      print "*"
    end

    # log = {
    #   src: pdf,
    #   showcase: jpg,
    #   macro: macro
    # }

    # マクロ名と、そのサイズオプションを記述する。
    optional_macros = {
      "#{macro}"=> "10.0cm",
      "#{macro}Xfourteen"=> "14.0cm",
      "#{macro}Xtwelve"=> "12.0cm",
      "#{macro}Xeight"=> "8.0cm",
      "#{macro}Xsix"=> "6.0cm",
    }

    optional_macros.each do |key, value|

      macro_name = key
      macro_width = value

      puts "\\#{macro_name}"
      caption = captions[macro] || macro_name

      template = """
        \\begin{figure}[htbp]
        \\centering
        \\includegraphics[width = #{macro_width}]{#{File.expand_path(pdf)}}
        \\caption{#{caption}}
        \\end{figure}
      """.gsub(" ", "").chomp

      results.push ""
      results.push "```"
      results.push "![#{macro_name}](#{File.expand_path(pdf)})"
      results.push "```"
      results.push ""
      results.push "```"
      results.push template
      results.push "```"
      results.push ""
      results.push "![](#{File.expand_path(jpg)})"


      macros.push ""
      macros.push "\\def"
      macros.push "\\#{macro_name}"
      macros.push "{"
      macros.push template
      macros.push "}\n\n"

    end


  end


  File.write config["dest"], results.join("\n")
  File.write config["macro"], macros.join("")
end
