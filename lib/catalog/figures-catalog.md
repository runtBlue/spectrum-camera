
```
![CameraSystem](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![CameraSystemXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.pdf}
\caption{デジタルカメラのシステム図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-camera-system@CameraSystem.jpg)

```
![DemosaicSystem](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DemosaicSystemXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.pdf}
\caption{デジタルカメラの撮像部から出力された生画像が、画像に生成されるまでの流れ図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-demosaic@DemosaicSystem.jpg)

```
![DiffBasic](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffBasicXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.pdf}
\caption{光が回折格子を通り分光されて、スクリーンに映し出される図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-basic@DiffBasic.jpg)

```
![DiffCondition](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![DiffConditionXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.pdf}
\caption{回折格子のガラス板に平行光線が入射し、進行方向に対して$\theta$方向に進むときの、回折格子の断面図。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-diff-condition@DiffCondition.jpg)

```
![CircleHHState](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![CircleHHStateXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.pdf}
\caption{水素の特性スペクトルの系列図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/basis-h2spectrum@CircleHHState.jpg)

```
![Bayer](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![BayerXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.pdf}
\caption{生画像に保存されている値の模式図。各画素にはRGBいずれかの値が格納されている不完全な平面データである。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bayer@Bayer.jpg)

```
![ByLinear](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![ByLinearXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.pdf}
\caption{バイリニア補間の模式図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/bylinear@ByLinear.jpg)

```
![BulbDenkyu](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![BulbDenkyuXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.pdf}
\caption{使用した白熱電球は、ソケットを使用して実験を行った。白熱電球は曇りガラスの60Wのものを使用した。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-d60-white@BulbDenkyu.jpg)

```
![DiffDevice](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![DiffDeviceXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.pdf}
\caption{実験系の装置全体のスケッチ図。回折格子分光器の回折格子部にデジタルカメラのレンズが接するように、配置して固定する。光源になるスペクトルランプあるいは電球を、カメラのレンズと回折格子分光器の入光部であるスリットの延長線上に配置する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-diffractation@DiffDevice.jpg)

```
![BulbLed](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLed}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXfourteen}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXfourteenH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXtwelve}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXtwelveH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXeight}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXeightH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXsix}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![BulbLedXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.pdf}
\caption{BulbLedXsixH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-led-white@BulbLed.jpg)

```
![PhotoKaisetsukoshi](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![PhotoKaisetsukoshiXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.pdf}
\caption{本研究で使用した回折格子分光器。ケニス株式会社の簡易分光器製作キットのもの。台形の立体形で、大きさは縦が約2600mm、短辺が450mm、長辺が1480mmのもの。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-PhotoKaisetsuKoshi@PhotoKaisetsukoshi.jpg)

```
![InstrumentPower](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentPowerXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.pdf}
\caption{エドモンド・オプティクス・ジャパン株式会社のスペクトルランプ（水素）とスペクトルランプ用電源。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-power-generator@InstrumentPower.jpg)

```
![InstrumentFromSlit](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentFromSlitXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.pdf}
\caption{}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-scale-slit@InstrumentFromSlit.jpg)

```
![InstrumentSocket](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocket}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXfourteen}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXfourteenH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXtwelve}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXtwelveH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXeight}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXeightH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXsix}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentSocketXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.pdf}
\caption{InstrumentSocketXsixH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-socket@InstrumentSocket.jpg)

```
![InstrumentMain](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentMainXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.pdf}
\caption{回折格子分光器を回折格子が取り付けられている窓から見たもの。前面の中央部は長方形に切り取られ、裏面から回折格子が取り付けられている。格子定数は500本/mmの透過型の回折格子が取り付けられている。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-spectruman-front@InstrumentMain.jpg)

```
![InstrumentDiffraction](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffraction}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXfourteen}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXfourteenH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXtwelve}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXtwelveH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXeight}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXeightH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXsix}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![InstrumentDiffractionXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.pdf}
\caption{InstrumentDiffractionXsixH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/dev-transparentum@InstrumentDiffraction.jpg)

```
![HydrogenSpectrumGraph](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.pdf}
\caption{水素の強度ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-focus-overview-for-thesis@HydrogenSpectrumGraph.jpg)

```
![HydrogenSpectrumGraphOverview](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![HydrogenSpectrumGraphOverviewXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.pdf}
\caption{水素スペクトルの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。それより右に順に、スペクトルラインを示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/h-overview-for-thesis@HydrogenSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverview](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraphOverviewXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.pdf}
\caption{電球のスペクトラムの強度ヒストグラムの全体。横軸はピクセルナンバー、縦軸は平均輝度を示す。RGB各色の輝度分布を重ねて示している。撮影したスペクトラムと同様に、右側が長波長である。真ん中のピークが０次回折光である。右の山が１次回折光を示している。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum-overview@DenkyuSpectrumGraphOverview.jpg)

```
![DenkyuSpectrumGraph](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumGraphXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.pdf}
\caption{電球のスペクトラムの輝度分布ヒストグラムから、直接光より右の一部を示した図}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-d60spectrum@DenkyuSpectrumGraph.jpg)

```
![DenkyuSpectrumPhotoCut](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoCutXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.pdf}
\caption{電球のスペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-cut@DenkyuSpectrumPhotoCut.jpg)

```
![DenkyuSpectrumPhotoEqual](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![DenkyuSpectrumPhotoEqualXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.pdf}
\caption{白色電球の撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-denkyu-png-eq@DenkyuSpectrumPhotoEqual.jpg)

```
![resfit](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![resfitXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.pdf}
\caption{画像のピクセル番号を水素の特性スペクトルを用いて較正した結果}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-fit.jpg)

```
![HydrogenSpectrumPhotoCut](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoCutXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.pdf}
\caption{水素スペクトルを撮影した画像から輝線部分を切り取ったもの}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-cut@HydrogenSpectrumPhotoCut.jpg)

```
![HydrogenSpectrumPhotoEqual](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![HydrogenSpectrumPhotoEqualXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.pdf}
\caption{水素の特性スペクトルの撮影から得られたデータに、画像処理を施して.png形式画像を生成した現像後の画像。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-hh-png-eq@HydrogenSpectrumPhotoEqual.jpg)

```
![resled](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resled}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXfourteen}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXfourteenH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXtwelve}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXtwelveH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXeight}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXeightH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXsix}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![resledXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.pdf}
\caption{resledXsixH}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/res-led.jpg)

```
![SystemViewFront](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=10.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXfourteen](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXfourteenH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=14.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXtwelve](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXtwelveH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=12.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXeight](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXeightH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=8.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXsix](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[htbp]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)

```
![SystemViewFrontXsixH](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf)
```

```

\begin{figure}[H]
\centering
\includegraphics[width=6.0cm]{/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.pdf}
\caption{奥の光源から発された光が、$50$cmほど離れた回折格子分光器の細いスリットを通る。分光器内を通って回折格子に達した光を、接するように配置したカメラが観測する。}
\end{figure}
```

![](/Users/hiroki/Documents/titech/11_Semester/graduating/working_space/figures/sysyem-view-front@SystemViewFront.jpg)