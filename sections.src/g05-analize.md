モザイクデータの現像
===
RAW形式ファイルの変換
---
デジタルカメラに保存されている .RW2 拡張子のついたファイルを取り出す。
このファイルは、通常保存される画像を生成する際に用いられた RAW形式ファイルの１つである。

RAW 形式ファイルには、通常＠＠。（要文献、cf. ノイズが発生したとしても、輝線の位置の決定と波長領域の決定の際に問題になり得ない。）
RAW 形式ファイルをファイル変換することで、人間の目で観察するのに適した画像が作成できる。このことを現像するという。

カメラから得られたRAW形式ファイルに変換処理を行い、PNG 画像を作成する。
変換処理には、多くのデジタルカメラの RAW 形式ファイルをファイル変換することができるオープンソースのフリーソフトウェア dcraw を用いる。
dcraw を用いて、RAW形式ファイルに対しバイリニア補間を用いたデモザイク処理を行い、ピクセルごとの成分値を R : G : B = １：１：１の比率の .ppm 形式ファイルを生成する。
ppm 形式ファイルとは、＠＠である。（要文献）

取得した ppm 形式ファイルを PNG 形式に非圧縮で変換する。
非圧縮で変換することで、ピクセルの値がカメラが観測した輝度に比例した値を保ったままの写真が得られる。変換後１ピクセルに各色 12bit の値が格納された PNG 画像を得ることができる。
変換するために、多くの画像ファイルをファイル変換することができるオープンソースのフリーソフトウェア IMAGEMAGICK を利用する。


変換の詳細手順
---
実験で得られた RAW形式ファイル TARGET_IMAGE.RW2 を、dcraw を用いて、ピクセルの値を R : G : B = １：１：１の比率でバイリニア補間を用いたデモザイク処理を行い、ppmファイル TARGET_IMAGE.ppm を生成する。

```
# TARGET_IMAGE.ppm を生成する。
$ dcraw -v +M -o 0 -q 3 -4  -g 1 1 -r 1 1 1 1 TARGET_IMAGE.RW2
```

ppmファイル TARGET_IMAGE.ppm を TARGET_IMAGE.png に非圧縮で変換する。

```
# TARGET_IMAGE.png を生成する。
$ convert TARGET_IMAGE.ppm TARGET_IMAGE.png
```


現像後の画像
---
<!-- 水素スペクトル現像写真、大きさそのまま -->
\HydrogenSpectrumPhotoEqualH
<!-- end -->

<!-- 電球スペクトラム現像写真、大きさそのまま -->
\DenkyuSpectrumPhotoEqualH
<!-- end -->

強度ヒストグラムの作成
===
スペクトラム画像の切り取り
---
現像後の画像には、画像中央部にスリットを通り抜けた光が直接カメラに届いた第０回析光があり、その左右に分光されたスペクトルが現れる。
外側のスペクトルがより長波長となる。
このスペクトル画像を横にカットし、スペクトルの輝度の値がはっきりと現れる部分だけのデータにする。

切り取り後の画像
---
切り取った画像は次のようになった。

<!-- 水素スペクトルカット画像 -->
\HydrogenSpectrumPhotoCutXfourteen
<!-- end -->

<!-- 電球スペクトラムカット画像 -->
\DenkyuSpectrumPhotoCutXfourteen
<!-- end -->

<!-- カットした後のLED電球のスペクトラム -->
<!-- end -->

強度ヒストグラムの作成
---
切り出した画像からピクセルごとにR, G, B 各色の成分を読み出し、縦１列の平均輝度をそれぞれの列毎に計算した。
クラフの横軸はピクセル位置、縦軸は相対強度を表す。



<!-- 水素スペクトルから得られたRGB強度ヒストグラムの一全体 -->
\HydrogenSpectrumGraphOverviewXfourteenH
<!-- end -->

<!-- 水素スペクトルから得られたRGB強度ヒストグラムの一部 -->
\HydrogenSpectrumGraphXfourteenH
<!-- end -->

<!-- 電球におけるRGB強度ヒストグラムの全体 -->
\DenkyuSpectrumGraphOverviewXfourteenH
<!-- end -->

<!-- 電球におけるRGB強度ヒストグラムの一部 -->
\DenkyuSpectrumGraphXfourteenH
<!-- end -->

### 強度ヒストグラムの作成方法の詳細手順

- 必要環境
  - dcraw
    - Dave Coffin氏が開発公開しているRAW現像ソフト
    - url: https://www.cybercom.net/~dcoffin/dcraw/
  - ImageMagick
    - ImageMagick（イメージマジック）は画像を操作したり表示したりするためのソフトウェアスイートである。
    - url: http://www.imagemagick.org/script/index.php


1. デジタルカメラから RAW 画像を取得
  DMC-LX5 Panasonic の場合は、RW2 データを取得できる。
  [Target Data] := @@@@@@.RW2 とする。
2. RAW形式ファイル to PNG
  - png-packer.rb を作成した。Ruby のスクリプトで以下の処理を自動化する。
  - RAW to ppm
    - $ dcraw -v +M -o 0 -q 3 -4  -g 1 1 -r 1 1 1 1 @@@@@@.RW2
    - 現像処理。補完処理も含まれている。
    - ここで -r が r : g : b の現像比を定めている。
  - ppm to png
    - $ convert @@@@@@.ppm @@@@@@.png
3. PNG 画像からスペクトルラインの切り取り
  - 省略
4. スペクトルライン画像から強度ヒストグラムの作成
  - pinstriper.rb を作成した。
  - Ruby のスクリプトで OpenCV 用いて画像のピクセル毎の値を読み出だせることができる。
  - スペクトルライン画像からピクセルごとにR, G, B 各色の成分を読み出し、縦１列の平均輝度をそれぞれの列毎に計算したものを CSV形式ファイルで出力する。

ヒストグラムから得られたデータ
===
ヒストグラムから、正確な位置、計数、スペクトルの半値全幅を記録した。

バックグラウンドは、R, G, B値共に 2.6程度あった。
そのため、輝度値 5.4 程度のピークはひとつの山としては見られなかった。

水素の強度ヒストグラムから、輝線のピクセル位置が、$2359, 2424, 2647$ px とわかった。


<!--ヒストグラムごとに、そこから読み取れた結果を、わかりやすい形にする。-->

Table: <!-- ここにはキャプションを改行を用いて追記してよい。 -->
水素の特性スペクトルの輝度分布のなかで、スペクトルのピークの値について示した図。
no datas はバックグラウンドに対して有意なピークが見られなかったことを示す。
輝度値は 12bit で求めた輝度値を 8bit 最大値 255 にしたものである。
ピーク位置とピークの半値位置はピクセルで表す。

----------------------------------------------------------------------
  色情報   スペクトル   輝度値   ピーク位置   半値位置
--------- ------------ -------- ------------ ------------------------
    B      青色         14.73     2359         2349, 2364

    G      青色         5.43      2359         no datas

    R      青色         no datas  no datas     no datas

    B      水色         87.17     2424         2419, 2427

    G      水色         72.04     2424         2418, 2427

    R      水色         13.02     2426         2423, 2427

    B      赤色         8.16      2646         2644, 2648

    G      赤色         14.7      2646         2644, 2648

    R      赤色         77.50     2647         2643, 2649

----------------------------------------------------------------------


<!-- このグラフは左寄せにしかならない。下に「：STRING」で記述したキャプションは上に記述される。 -->
+----------------+---------------+
| background lumix               |
+================+===============+
|   blue left    |   3.5         |
|                |               |
+----------------+---------------+
|   green left   |   3.5         |
|                |               |
+----------------+---------------+
|   green right  |   2.8         |
|                |               |
+----------------+---------------+
|   red right    |   2.8         |
|                |               |
+----------------+---------------+

: 輝度分布から、輝度分布の山の立ち上がり、立ち下がり時のバックグラウンド値


: 電球の連続スペクトラムの各色の分布領域

----------------------------------------------------------------------
  color    lumix      peak value      half range        1 / 20 range
-------- ---------- ------------- ------------------ -----------------
   B       85.00       2412        2353( to 2458)       2333( to 2574)

   G       142.59      2485.76     2421 to 2560         2345 to 2646

   R       117.89      2562        (2540 to )2626       (2437 to )2674

----------------------------------------------------------------------




<!-- 画像と波長の較正 -->
画像のピクセル番号の較正
===
水素のスペクトラム
---
水素原子のスペクトラムは、

$$ En \sim - \frac{13.6}{n ^ 2} $$

で与えられる。
測定系の水素スペクトルは可視光領域なので、バルマー系列であり、それぞれのスペクトルは波長が短い順に 434, 486, 656 nm であると考えられる。

ピクセル番号の較正
---
実験から得られた水素スペクトラムの輝線のピクセル値と、そこでのバルマー系列の波長値を線形対応させて、測定系におけるスペクトルと波長の較正を得た。

<!-- スペクトルとピクセル位置の較正 -->
\resfitH
<!-- end -->

: スペクトルとピクセル位置の線形較正結果

+---------------+---------------+
| 線形フィット結果              |
+===============+===============+
| Chi2          | 2.17765       |
|               |               |
+---------------+---------------+
| NDf           | 1             |
|               |               |
+---------------+---------------+
| p0            | -1378.34      |
|               |               |
+---------------+---------------+
| p1            | 0.768645      |
|               |               |
+---------------+---------------+


スペクトル領域の考察
===


バックグラウンド
---

「？」山の立ち上がりと立ち下がりを見ると、有意な分布の外側にはバックグラウンドが存在している。

スリットの抜けて直接回折格子を通した光は分光されるが、このバックグラウンドは広い範囲にわたってなだらかに値を示している。
このことから、回折格子分光器内に満遍なく照らされた光が反射して回折格子を通してカメラによって観測された、もしくは暗室での実験ではあるが実験時に光源によって照らされた部屋の環境光が測定されたと考えることができる。

測定可能領域の考察
---
光の測定可能な領域を決定するために、特定の色の分布の形から判断をする。
デジタルカメラの光の感度の設定次第では、輝度値は比例して変化する。
そのため、どの感度設定においても山の形は同じになる。
今回は分布の山の最大値に対して、山の立ち上がりと立ち下がりの比率を比較して、測定可能な領域の決定を行う。

電球から得たスペクトラムの RGB各色の分布が、そのピークの値に対して 5％ の値を示していれば、測定できていると仮定する。
デジタルカメラは B, G, R 各色の測定を行い、それらを重ね合わせたデータを出力する。低波長領域は B、超波長領域は R が広い測定範囲を持つため、この２つの測定可能範囲を求めて、２つの測定可能範囲がデジタルカメラによる測定可能範囲であるとする。

この分布の５％を決定する際には、バックグラウンドを除いた分布の形に対して考察を行った。

以上のように仮定した上で、B が測定できた範囲は 2333 px から 2574 px 、R が測定できている範囲は 2437 px から 2674 px であった。

波長較正
---
水素の可視光領域における特性スペクトルの波長と、測定によって得られた水素の特性スペクトルのピクセル位置の較正結果を用いて、スペクトルが撮影されたピクセル位置から、そのスペクトルの波長を換算する式を求めることができる。

式や較正の説明挿入するか未定＜＜wave_length_nm = px_to_wave_length_nm (px) -> -1378.34 + 0.768645 * px ＞＞

ピクセル値から波長(nm) へ変換するためには、 A ピクセル値に対して -1378.34 + 0.768645 * A を求めると、その位置におけるスペクトルの波長(nm)を求めることができる。

測定可能な領域の決定
---
B が測定できた 2333 px から 2574 px の領域は、波長に換算して 414.91 nm から 600.15 nm である。
R が測定できた 2437 px から 2674 px の領域は、波長に換算して 494.85 nm から 677.02 nm である。
